
TEMPLATE = subdirs

SUBDIRS = \
      krakenapi \
      controlgui \

CONFIG += ordered

krakenapi.subdir    = src/krakenapi
controlgui.subdir   = src/controlgui

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <krakenapi.h>
#include <krakenapisession.h>
#include <krakenmarketapi.h>
#include <krakendatalogger.h>
#include <ui/krakenchart.h>
#include <QVBoxLayout>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    kraken_(new KrakenAPI()),
    krakenChart_(new KrakenChart()),
    currentMarket_(nullptr),
    krakenDataLogger_(new KrakenDataLogger())
{
    Q_ASSERT(kraken_);
    Q_ASSERT(krakenChart_);
    Q_ASSERT(krakenDataLogger_);
    ui->setupUi(this);

    connect(kraken_, SIGNAL(debug(QString)), this, SLOT(log(QString)));
    connect(kraken_, SIGNAL(ready()), this, SLOT(onKrakenReady()));
    connect(kraken_, SIGNAL(failed(QString)), this, SLOT(onKrakenFailed(QString)));
    connect(kraken_, SIGNAL(createdDefaultConfigFile()), this, SLOT(onKrakenDefaultedConfig()));

    connect(krakenDataLogger_, SIGNAL(debug(QString)), this, SLOT(log(QString)));
    connect(krakenDataLogger_, SIGNAL(failed(QString)), this, SLOT(onDataLoggerFailed(QString)));
    connect(krakenDataLogger_, SIGNAL(parseError(QString)), this, SLOT(onDataLoggerParseError(QString)));
    connect(krakenDataLogger_, SIGNAL(finished()), this, SLOT(onDataLoggerFinished()));

    QVBoxLayout* layoutChart = new  QVBoxLayout();
    layoutChart->addWidget(krakenChart_);
    ui->wChartContainer->setLayout(layoutChart);

    if (!kraken_->loadConfig())
        log(QString("kraken failed to loadConfig: %1").arg(kraken_->lastError()));
}

MainWindow::~MainWindow()
{
    if (krakenChart_)
        {delete krakenChart_; krakenChart_ = nullptr;}
    if (kraken_)
        {delete kraken_; kraken_ = nullptr;}
    delete ui;
}

void MainWindow::log(QString msg)
{
    ui->txtLog->appendPlainText(msg);
}

void MainWindow::setCurrentMarket(KrakenMarketAPI* market)
{
    Q_ASSERT(kraken_);
    Q_ASSERT(krakenChart_);
    if (currentMarket_)
    {
        disconnect(currentMarket_, SIGNAL(debug(QString)), this, SLOT(log(QString)));
        disconnect(currentMarket_, SIGNAL(failed(QString)), this, SLOT(onMarketFailed(QString)));
        disconnect(currentMarket_, SIGNAL(parseError(QString)), this, SLOT(onMarketParseError(QString)));
        disconnect(currentMarket_, 0, krakenDataLogger_, 0);
    }
    currentMarket_ = market;
    if (currentMarket_)
    {
        connect(currentMarket_, SIGNAL(debug(QString)), this, SLOT(log(QString)));
        connect(currentMarket_, SIGNAL(failed(QString)), this, SLOT(onMarketFailed(QString)));
        connect(currentMarket_, SIGNAL(parseError(QString)), this, SLOT(onMarketParseError(QString)));
        krakenChart_->setMarket(currentMarket_);
    }
}

void MainWindow::onKrakenReady()
{
    log("Kraken Ready!");
}

void MainWindow::onKrakenDefaultedConfig()
{
    log("kraken defaulted config");
}

void MainWindow::onKrakenFailed(QString msg)
{
    log(QString("kraken failed: %1").arg(msg));
}

void MainWindow::onMarketFailed(QString msg)
{
    KrakenMarketAPI* market = qobject_cast<KrakenMarketAPI*>(sender());
    log(QString("market %1 failed: %2").arg( (market ? market->marketPair() : "<unknown>") ).arg(msg));
}

void MainWindow::onMarketParseError(QString msg)
{
    KrakenMarketAPI* market = qobject_cast<KrakenMarketAPI*>(sender());
    log(QString("market %1 parse error: %2").arg( (market ? market->marketPair() : "<unknown>") ).arg(msg));
}

void MainWindow::onDataLoggerFailed(QString msg)
{
    log(QString("Data Logger failed: %1").arg(msg));
}

void MainWindow::onDataLoggerParseError(QString msg)
{
    log(QString("Data Logger parse error: %1").arg(msg));
}

void MainWindow::onDataLoggerFinished()
{
    log(QString("Data Logger finished (importing data)"));
}

void MainWindow::on_btnStart_clicked()
{
    Q_ASSERT(kraken_);
    log("Starting kraken session");
    if (!kraken_->start())
    {
        log(QString("kraken start failed: %1").arg(kraken_->lastError()));
        return;
    }
}

void MainWindow::on_btnStartPublicMontor_clicked()
{
    Q_ASSERT(kraken_);
    log("Starting kraken session");
    if (!kraken_->session()->openMonitor())
    {
        log(QString("kraken session openMonitor failed: %1").arg(kraken_->session()->lastError()));
        return;
    }
}

void MainWindow::on_btnStop_clicked()
{
    Q_ASSERT(kraken_);
    log("Stopping kraken session");
    kraken_->stop();
}

void MainWindow::on_btnListMarkets_clicked()
{
    Q_ASSERT(kraken_);
    //kraken_->session()->request("");
}

void MainWindow::on_btnCreateMarket_clicked()
{
    Q_ASSERT(kraken_);
    KrakenMarketAPI* market = kraken_->getMarket(ui->txtCreateMarketPair->text(), true/*addMissing*/);
    if (!market)
    {
        log(QString("kraken failed to getMarket '%1': %2").arg(ui->txtCreateMarketPair->text()).arg(kraken_->lastError()));
        return;
    }
    setCurrentMarket(market);
}

void MainWindow::on_btnSubscribeCandles_clicked()
{
    if (!currentMarket_)
    {
        log("Subscribe Candles No current market selected");
        return;
    }
    if (!currentMarket_->subscribe("ohlc"))
    {
        log(QString("Current market %1 failed to subscribe to candles: %2").arg(currentMarket_->marketPair()).arg(currentMarket_->lastError()));
        return;
    }
}

void MainWindow::on_btnStartDataLogger_clicked()
{
    Q_ASSERT(krakenDataLogger_);
    if (!currentMarket_)
    {
        log("Start Data Logger No current market selected to log");
        return;
    }
    if (ui->txtDataLogFilename->text().trimmed().isEmpty())
    {
        log("Start Data Logger no data log filename specified");
        return;
    }
    krakenDataLogger_->setMarket(currentMarket_);
    if (!krakenDataLogger_->logData(false/*leaveOpen*/, ui->txtDataLogFilename->text().trimmed()))
    {
        log(QString("Start Data Logger krakenDataLogger logData failed: %1").arg(krakenDataLogger_->lastError()));
        disconnect(currentMarket_, 0, krakenDataLogger_, 0);
        return;
    }
    log(QString("Data Logger started for market '%1'").arg(currentMarket_->marketPair()));
}

void MainWindow::on_btnLoadDataLog_clicked()
{
    Q_ASSERT(krakenDataLogger_);
    if (!currentMarket_)
    {
        log("Load Data Logger No current market selected");
        return;
    }
    krakenDataLogger_->setMarket(currentMarket_);
    if (!krakenDataLogger_->loadData(0/*delayMs*/, ui->txtDataLogFilename->text().trimmed()))
    {
        log(QString("Load Data Logger krakenDataLogger loadData failed: %1").arg(krakenDataLogger_->lastError()));
        return;
    }

    log(QString("Load Data Logger loaded or started loading market '%1'").arg(currentMarket_->marketPair()));
}

void MainWindow::on_btnStopDataLogger_clicked()
{
    Q_ASSERT(krakenDataLogger_);

    disconnect(currentMarket_, 0, krakenDataLogger_, 0);
    log("Data Logger stopped (loading/logging)");
}

void MainWindow::on_btnBrowseDataLogs_clicked()
{

    QString defFilter = "txt";
    QString fileName = QFileDialog::getSaveFileName(this, "Choose a data log filename",
                               ui->txtDataLogFilename->text().trimmed(),
                               "Data Log (*.dat *.log *.txt)", &defFilter, QFileDialog::DontConfirmOverwrite);
    if (!fileName.isEmpty())
        ui->txtDataLogFilename->setText(fileName);
}

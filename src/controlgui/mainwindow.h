#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class KrakenAPI; // predeclared
class KrakenChart; // predeclared
class KrakenMarketAPI; // predeclared
class KrakenDataLogger; // predeclared

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setCurrentMarket(KrakenMarketAPI* market);

private slots:
    void on_btnStart_clicked();
    void on_btnStop_clicked();
    void on_btnListMarkets_clicked();
    void on_btnCreateMarket_clicked();
    void on_btnSubscribeCandles_clicked();
    void on_btnStartDataLogger_clicked();
    void on_btnStopDataLogger_clicked();
    void on_btnBrowseDataLogs_clicked();
    void on_btnLoadDataLog_clicked();

    void on_btnStartPublicMontor_clicked();

private:
    Ui::MainWindow *ui;
    KrakenAPI* kraken_;
    KrakenChart* krakenChart_;
    KrakenMarketAPI* currentMarket_;
    KrakenDataLogger* krakenDataLogger_;

public slots:
    void log(QString msg);

    void onKrakenReady();
    void onKrakenDefaultedConfig();
    void onKrakenFailed(QString msg);

    void onMarketFailed(QString msg);
    void onMarketParseError(QString msg);

    void onDataLoggerFailed(QString msg);
    void onDataLoggerParseError(QString msg);
    void onDataLoggerFinished(); // finished importing data

};
#endif // MAINWINDOW_H


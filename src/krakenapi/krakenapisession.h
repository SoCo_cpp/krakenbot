#ifndef KRAKENAPISESSION_H
#define KRAKENAPISESSION_H

#include <QObject>
#include <QJsonDocument>

class KrakenREST; // predeclared
class KrakenWS; // predeclared

class KrakenAPISession : public QObject
{
    Q_OBJECT
public:
    explicit KrakenAPISession(QObject* parent = nullptr);
    ~KrakenAPISession();

    const QString& lastError() const;
    void debugLog(const QString& msg);

    KrakenREST* rest();
    KrakenWS* publicWebSocket();
    KrakenWS* privateWebSocket();

    void setUrlsREST(const QString& urlPublic, const QString& urlPrivate);
    void setUrlsWS(const QString& urlPublic, const QString& urlPrivate);
    void setAuthKeys(const QString& pubKey, const QString& privKey);

    bool start();
    void stop();
    bool isReady();

    bool openMonitor();
    void closeMonitor();
    bool monitorIsOpen();

    bool authenticate();
    bool isAuthenticated();
    bool openAuthenticated(const QString& authToken);
    void closeAuthenticated();
    bool authenticatedIsOpen();

    const QString& authToken() const;

    bool subscribe(const QString& channelName, bool isPublic = true, const QString& marketPair = QString(), int reqid = -1, int depth = -1, int interval = -1, bool snapshot = true/*default*/);
    bool unsubscribe(const QString& channelName, bool isPublic = true, const QString& marketPair = QString(), int reqid = -1, int depth = -1, int interval = -1);

    // REST
    bool request(quint32 reqid, bool isPublic, const QString& command, const QMap<QString,QString>& params);
    bool request(quint32 reqid, bool isPublic, const QString& command, const QString& paramAssignments);

    // WS
    bool sendMessage(bool isPublic, const QJsonDocument& jsonMessage);
    bool sendMessage(bool isPublic, const QString& stringMessage);

protected:
    bool isReady_;
    KrakenREST* rest_;
    KrakenWS* wsPublic_;
    KrakenWS* wsPrivate_;
    QString authToken_;
    QString lastError_;

signals:
    void gotAuthToken();
    void authenticated();

    void debug(QString msg);
    void failed(QString errorMessage);
    void restFailed(quint32 reqid, QString errorMessage);
    void monFailed(QString errorMessage);
    void authFailed(QString errorMessage);

    void restMessage(quint32 reqid, QJsonDocument msg);
    void monMessage(QJsonDocument msg);
    void authMessage(QJsonDocument msg);

    void ready();
    void notReady();
public slots:
    void onRestFailed(quint32 reqid, QString errorMessage);
    void onMonFailed(QString errorMessage);
    void onAuthFailed(QString errorMessage);
    void restReply(quint32 reqid, QJsonDocument jsonReply);
    void wsAuthConnected();
    void wsDisconnected();
};

#endif // KRAKENAPISESSION_H

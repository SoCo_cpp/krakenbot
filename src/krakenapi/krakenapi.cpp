#include "krakenapi.h"
#include "krakenmarketapi.h"
#include "krakenapisession.h"
#include "krakenrest.h"
#include "krakenws.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QStandardPaths>
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QSettings>

KrakenAPI::KrakenAPI(KrakenAPISession* session /*= nullptr*/, QObject* parent /*= nullptr*/) :
    QObject(parent),
    session_(nullptr),
    config_(nullptr),
    dataPath_("")
{
    setDataDirectory("krakenapi");
    setSession(session);
}

KrakenAPI::~KrakenAPI()
{
    if (ownSession_ && session_)
    {
        ownSession_ = false;
        delete session_;
        session_ = nullptr;
    }
    if (config_)
        {delete config_;config_ = nullptr;}
}

const QString& KrakenAPI::lastError() const
{
    return lastError_;
}

bool KrakenAPI::loadConfig(bool createSkeleton /*= true*/, QString fileName /*= QString()*/)
{
    QFileInfo fileInfo;
    QDir dir(dataPath_);
    if (config_)
        {delete config_;config_ = nullptr;}
    if (dataPath_.isEmpty())
    {
        lastError_ = "loadConfig config/data path not set or detected";
        return false;
    }
    if (!dir.exists())
    {
        if (!createSkeleton)
        {
            lastError_ = QString("loadConfig data directory doesn't exist, failing: %1").arg(dataPath_);
            return false;
        }
        debugLog(QString("loadConfig data directory doesn't exist, creating it: %1").arg(dataPath_));
        dir.mkpath(dataPath_);
    }
    if (fileName.isEmpty())
        fileName = "krakenapi.cfg";
    fileInfo.setFile(dir, fileName);
    if (!fileInfo.exists())
    {
        if (!createSkeleton)
        {
            lastError_ = QString("loadConfig config file doesn't exist, failing: %1").arg(fileInfo.filePath());
            return false;
        }
        debugLog(QString("loadConfig config file doesn't exist, creating skeleton: %1").arg(fileInfo.filePath()));
        QFile file(fileInfo.filePath());
        if (!file.open(QIODevice::ReadWrite))
        {
            lastError_ = QString("loadConfig config file not writeable, failing: %1").arg(fileInfo.filePath());
            return false;
        }
        file.close();
        Q_ASSERT(session_);
        config_ = new QSettings(fileInfo.filePath(), QSettings::IniFormat);
        Q_ASSERT(config_);
        if (!saveConfig())
        {
            lastError_ = QString("loadConfig create skeleton saveConfig failed: %1").arg(lastError_);
            return false;
        }
        emit createdDefaultConfigFile();
        return true;
    }
    debugLog(QString("loadConfig loading from: %1").arg(fileInfo.filePath()));
    config_ = new QSettings(fileInfo.filePath(), QSettings::IniFormat);
    Q_ASSERT(config_);
    config_->beginGroup("KrakenAPI_URLs");
    config_->beginGroup("REST");
    if (!config_->contains("Public") || !config_->contains("Private"))
    {
        lastError_ = "loadConfig config missing required fields KrakenAPI/API_URL/REST/Public or KrakenAPI/API_URL/REST/Public";
        return false;
    }
    setUrlsREST(config_->value("Public").toString(), config_->value("Private").toString());
    config_->endGroup(); // REST
    config_->beginGroup("WebSockets");
    if (!config_->contains("Public") || !config_->contains("Private"))
    {
        lastError_ = "loadConfig config missing required fields KrakenAPI/API_URL/WebSockets/Public or KrakenAPI/API_URL/WebSockets/Public";
        return false;
    }
    setUrlsWS(config_->value("Public").toString(), config_->value("Private").toString());
    config_->endGroup(); // WebSockets
    config_->endGroup(); // KrakenAPI_URLs
    config_->beginGroup("KrakenAPI_Authentication");
    if (!config_->contains("PublicKey") || !config_->contains("PrivateKey"))
    {
        lastError_ = "loadConfig config missing required fields KrakenAPI/Authentication/PublicKey or KrakenAPI/Authentication/PrivateKey";
        return false;
    }
    setAuthKeys(config_->value("PublicKey").toString(), config_->value("PrivateKey").toString());
    config_->endGroup(); // KrakenAPI_Authentication
    return true;
}

bool KrakenAPI::saveConfig()
{
    if (!config_)
    {
        lastError_ = "saveConfig config not ready";
        return false;
    }
    config_->beginGroup("KrakenAPI_Authentication");
        config_->setValue("PublicKey", "<ENTER PUBLIC API KEY HERE>");
        config_->setValue("PrivateKey", "<ENTER PRIVATE API KEY HERE>");
    config_->endGroup();
    config_->beginGroup("KrakenAPI_URLs");
        config_->beginGroup("REST");
            config_->setValue("Public", session_->rest()->urlPublic().toString());
            config_->setValue("Private", session_->rest()->urlPrivate().toString());
        config_->endGroup();
        config_->beginGroup("WebSockets");
            config_->setValue("Public", session_->publicWebSocket()->url().toString());
            config_->setValue("Private", session_->privateWebSocket()->url().toString());
        config_->endGroup();
    config_->endGroup();
    config_->sync();
    return true;
}

QSettings* KrakenAPI::getConfig()
{
    return config_; // may be null
}

void KrakenAPI::setDataPath(const QString& fullPath)
{
    dataPath_ = fullPath;
}

void KrakenAPI::setDataDirectory(const QString& endDirectory)
{
    dataPath_ = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    if (!dataPath_.isEmpty())
        dataPath_ = QDir::cleanPath(QString("%1%2%3").arg(dataPath_).arg(QDir::separator()).arg(endDirectory));
    else
        dataPath_ = endDirectory;
}

const QString& KrakenAPI::dataPath() const
{
    return dataPath_;
}

void KrakenAPI::debugLog(const QString& msg)
{
    emit debug(msg);
}

void KrakenAPI::setUrlsREST(const QString& urlPublic, const QString& urlPrivate)
{
    Q_ASSERT(session_);
    session_->setUrlsREST(urlPublic, urlPrivate);
}

void KrakenAPI::setUrlsWS(const QString& urlPublic, const QString& urlPrivate)
{
    Q_ASSERT(session_);
    session_->setUrlsWS(urlPublic, urlPrivate);
}

void KrakenAPI::setAuthKeys(const QString& pubKey, const QString& privKey)
{
    Q_ASSERT(session_);
    session_->setAuthKeys(pubKey, privKey);
}

void KrakenAPI::setSession(KrakenAPISession* session /*= nullptr*/)
{
    if (session_)
    {
        if (ownSession_)
        {
            delete session_;
            session_ = nullptr;
        }
        else disconnect(session_);
    }
    ownSession_ = false;
    session_ = session;
    if (!session_)
    {
        ownSession_ = true;
        session_ = new KrakenAPISession();
    }
    Q_ASSERT(session_);
    connect(session_, SIGNAL(failed(QString)), this, SLOT(sessionFailed(QString)));
    connect(session_, SIGNAL(failed(QString)), this, SIGNAL(failed(QString)));
    connect(session_, SIGNAL(restFailed(quint32,QString)), this, SIGNAL(reqestFailed(quint32,QString)));
    connect(session_, SIGNAL(restMessage(quint32,QJsonDocument)), this, SIGNAL(requestMessage(quint32,QJsonDocument)));
    connect(session_, SIGNAL(monMessage(QJsonDocument)), this, SIGNAL(monMessage(QJsonDocument)));
    connect(session_, SIGNAL(authMessage(QJsonDocument)), this, SIGNAL(authMessage(QJsonDocument)));
    connect(session_, SIGNAL(monMessage(QJsonDocument)), this, SIGNAL(message(QJsonDocument)));
    connect(session_, SIGNAL(authMessage(QJsonDocument)), this, SIGNAL(message(QJsonDocument)));

    connect(session_, SIGNAL(ready()), this, SIGNAL(ready()));
    connect(session_, SIGNAL(debug(QString)), this, SIGNAL(debug(QString)));
    // setSession for each of markets_ list ?
}

bool KrakenAPI::sessionReady() const
{
    Q_ASSERT(session_);
    return session_->isReady();
}

KrakenAPISession* KrakenAPI::session()
{
    Q_ASSERT(session_);
    return session_;
}

const KrakenAPISession* KrakenAPI::session() const
{
    Q_ASSERT(session_);
    return session_;
}

void KrakenAPI::sessionFailed(QString errorMessage)
{
    Q_UNUSED(errorMessage);
    // handle specific fialures
}

bool KrakenAPI::start()
{
    Q_ASSERT(session_);
    if (!session_->start())
    {
        lastError_ = QString("start session start failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

void KrakenAPI::stop()
{
    Q_ASSERT(session_);
    session_->stop();
}

bool KrakenAPI::isReady()
{
    return sessionReady();
}

KrakenMarketAPI* KrakenAPI::getMarket(const QString& marketPair, bool addMissing /*= false*/)
{
    Q_ASSERT(session_);
    if (markets_.keys().contains(marketPair))
        return markets_[marketPair];
    if (!addMissing)
    {
        lastError_ = QString("getMarket market '%1' does not exist").arg(marketPair);
        return nullptr;
    }
    KrakenMarketAPI* newMarket = new KrakenMarketAPI(marketPair, session_);
    Q_ASSERT(newMarket);
    connect(newMarket, SIGNAL(debug(QString)), this, SIGNAL(debug(QString)));
    markets_[marketPair] = newMarket;
    return  markets_[marketPair];
}

bool KrakenAPI::deleteMarket(const QString& marketPair, bool failNoExists /*= true*/)
{
    if (!markets_.keys().contains(marketPair))
    {
        if (failNoExists)
            return false; // doesn't exist, fail
        return true; // doesn't exist, successs
    }
    KrakenMarketAPI* delMarket = markets_.take(marketPair);
    Q_ASSERT(delMarket);
    delete delMarket;
    return true;
}

bool KrakenAPI::subscribe(const QString& channelName, bool isPublic /*= true*/, int reqid /*= -1*/, int depth /*= -1*/, int interval /*= -1*/, bool snapshot /*= true default*/)
{
    Q_ASSERT(session_);
    if (!session_->subscribe(channelName, isPublic, QString()/*marketPair*/, reqid, depth, interval, snapshot))
    {
        lastError_ = QString("unsubscribe session subscribe failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPI::unsubscribe(const QString& channelName, bool isPublic /*= true*/, int reqid /*= -1*/, int depth /*= -1*/, int interval /*= -1*/)
{
    Q_ASSERT(session_);
    if (!session_->unsubscribe(channelName, isPublic, QString()/*marketPair*/, reqid, depth, interval))
    {
        lastError_ = QString("unsubscribe session unsubscribe failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPI::tradeBalance(quint32 reqid, const QString& assetName)
{
    Q_ASSERT(session_);
    if (!session_->request(reqid, false/*isPublic*/, "TradeBalance", QString("asset=%1").arg(assetName)))
    {
        lastError_ = QString("tradeBalance session request failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPI::assetInfo(quint32 reqid, const QString& assetName /*= QString() all*/)
{
    Q_ASSERT(session_);
    QString params;
    if (!assetName.isEmpty())
        params = QString("asset=%1").arg(assetName);
    if (!session_->request(reqid, true/*isPublic*/, "Assets", params))
    {
        lastError_ = QString("assetInfo session request failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPI::assetInfo(quint32 reqid, const QStringList& assetNames)
{
    Q_ASSERT(session_);
    QString params;
    if (!assetNames.isEmpty())
        params = QString("asset=%1").arg(assetNames.join(','));
    if (!session_->request(reqid, true/*isPublic*/, "Assets", params))
    {
        lastError_ = QString("assetInfo session request failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPI::assetPairs(quint32 reqid, const QString& assetPair /*= QString() all*/, const QString& info /*= QString() all*/)
{
    Q_ASSERT(session_);
    QMap<QString,QString> params;
    if (!assetPair.isEmpty())
        params["pair"] = assetPair;
    if (!info.isEmpty())
        params["info"] = info;
    if (!session_->request(reqid, true/*isPublic*/, "AssetPairs", params))
    {
        lastError_ = QString("assetPairs session request failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPI::assetPairs(quint32 reqid, const QStringList& assetPairs, const QString& info /*= QString() all*/)
{
    Q_ASSERT(session_);
    QMap<QString,QString> params;
    if (!assetPairs.isEmpty())
        params["pair"] = assetPairs.join(',');
    if (!info.isEmpty())
        params["info"] = info;
    if (!session_->request(reqid, true/*isPublic*/, "AssetPairs", params))
    {
        lastError_ = QString("assetPairs session request failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPI::cancelAllOrders(int reqid/* = -1*/)
{
    Q_ASSERT(session_);
    QJsonDocument json;
    QJsonObject jo;
    QJsonArray ja;
    if (!session_->isReady())
    {
        lastError_ = QString("cancelAllOrders session not ready");
        return false;
    }
    jo["event"] = "cancelAll";
    jo["token"] = session_->authToken();
    if (reqid != -1)
        jo["reqid"] = reqid;
    json.setObject(jo);
    if (!session_->sendMessage(false/*isPublic*/, json))
    {
        lastError_ = QString("cancelAllOrders session sendMessage (private) failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

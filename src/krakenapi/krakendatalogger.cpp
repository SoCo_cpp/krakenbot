#include "krakendatalogger.h"
#include "krakenmarketapi.h"
#include <QFile>
#include <QTimer>
#include <QDateTime>
#include <QVector>
#include <QStringRef>
#include <QtMath>


KrakenDataLogger::KrakenDataLogger(const QString& fileName /*= QString()*/, QObject* parent /*= nullptr*/) :
    QObject(parent),
    logging_(false),
    loading_(false),
    leaveOpenLogging_(true),
    format_(C_Default_File_Format),
    fileName_(fileName),
    fileVersion_(0),
    fileLine_(0),
    lastTimeOHLC_(0.0),
    lastOutEntryOHLC_(""),
    market_(nullptr),
    multiMarket_(false),
    marketPair_(""),
    file_(new QFile()),
    loadDelayTimer_(new QTimer()),
    flushLogTimer_(new QTimer())
{
    Q_ASSERT(file_);
    Q_ASSERT(loadDelayTimer_);
    Q_ASSERT(flushLogTimer_);
}

KrakenDataLogger::~KrakenDataLogger()
{
    if (flushLogTimer_)
        {flushLogTimer_->stop();delete flushLogTimer_;flushLogTimer_ = nullptr;}
    if (loadDelayTimer_)
        {loadDelayTimer_->stop();delete loadDelayTimer_;loadDelayTimer_ = nullptr;}
    if (file_)
        {delete file_;file_ = nullptr;}
}

const QString& KrakenDataLogger::lastError() const
{
    return lastError_;
}

void KrakenDataLogger::debugLog(const QString& msg)
{
    emit debug(msg);
}

void KrakenDataLogger::setFormat(const QString& format)
{
    format_ = format;
}

const QString& KrakenDataLogger::format() const
{
    return format_;
}

void KrakenDataLogger::setFileName(const QString& fileName)
{
    fileName_ = fileName;
}

const QString& KrakenDataLogger::fileName() const
{
    return fileName_;
}

bool KrakenDataLogger::isActive() const
{
    return (logging_ || loading_);
}

bool KrakenDataLogger::isLogging() const
{
    return logging_;
}

bool KrakenDataLogger::isLoading() const
{
    return loading_;
}

void KrakenDataLogger::setMarket(KrakenMarketAPI* market)
{
    debugLog("Data Logger setting market...");
    if (market_)
    {
        debugLog(QString("Data Logger Disconnecting from market '%1'").arg(market_->marketPair()));
        disconnect(market_, SIGNAL(eventOHLC(double,double,double,double,double,double,double,double,int)), this, SLOT(inputOHLC(double,double,double,double,double,double,double,double,int)));
        disconnect(this, SIGNAL(outputOHLC(double,double,double,double,double,double,double,double,int)), market_, SIGNAL(eventOHLC(double,double,double,double,double,double,double,double,int)));
    }
    market_ = market;
    if (market_)
    {
        debugLog(QString("Data Logger connecting to market '%1'").arg(market_->marketPair()));
        connect(market_, SIGNAL(eventOHLC(double,double,double,double,double,double,double,double,int)), this, SLOT(inputOHLC(double,double,double,double,double,double,double,double,int)));
        connect(this, SIGNAL(outputOHLC(double,double,double,double,double,double,double,double,int)), market_, SIGNAL(eventOHLC(double,double,double,double,double,double,double,double,int)));
        setMarketPair(market_->marketPair());
    }
    else
    {
        debugLog("Data Logger setMarket to none");
        setMarketPair("");
    }
}

KrakenMarketAPI* KrakenDataLogger::market()
{
    return market_;
}

void KrakenDataLogger::setMultiMarket(bool multi /*= true*/)
{
    multiMarket_ = multi;
}

bool KrakenDataLogger::multiMarket() const
{
    return multiMarket_;
}

void KrakenDataLogger::setMarketPair(const QString& marketPair)
{
    marketPair_ = marketPair;
    emit marketPairChanged(marketPair_);
}

const QString& KrakenDataLogger::marketPair() const
{
    return marketPair_;
}

bool KrakenDataLogger::logData(const QString& format /*= QString()*/, bool leaveOpen /*= true*/, const QString& fileName /*= QString()*/)
{
    Q_ASSERT(file_);
    Q_ASSERT(flushLogTimer_);
    if (isActive())
    {
        lastError_ = "logData already active";
        return false;
    }
    if (file_->isOpen())
        file_->close();
    if (!format.isEmpty())
        format_ = format;
    if (!fileName.isEmpty())
        fileName_ = fileName;
    if (fileName_.isEmpty())
    {
        lastError_ = "logData file name not set";
        return false;
    }
    file_->setFileName(fileName_);
    logging_ = true;
    lastTimeOHLC_ = 0.0;
    lastOutEntryOHLC_ = "";
    if (!scanLastFileState())
    {
        lastError_ = QString("logData scanLastFileState failed: %1").arg(lastError_);
        stop();
        return false;
    }
    if (!file_->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
    {
        lastError_ = QString("logData open file for writing failed (%1): %2").arg(fileName_).arg(file_->errorString());
        stop();
        return false;
    }
    if (format_ == "custom")
    {
        if (fileVersion_ != C_Current_File_Version)
        {
            fileVersion_ = C_Current_File_Version;

            if (!file_->write(QString("ver,%1,%2\n").arg(QDateTime::currentMSecsSinceEpoch()).arg(fileVersion_).toLocal8Bit()))
            {
                lastError_ = QString("logData write header failed (%1): %2").arg(fileName_).arg(file_->errorString());
                stop();
                return false;
            }
        }
    }
    else if (format_ == "simply-csv")
    {
        if (fileLine_ == 0)
            if (!file_->write("date,open,high,low,close,volume\n"))
            {
                lastError_ = QString("logData write header failed (%1): %2").arg(fileName_).arg(file_->errorString());
                stop();
                return false;
            }
    }
    leaveOpenLogging_ = leaveOpen;
    if (leaveOpenLogging_)
        flushLogTimer_->start(5000);
    else
        file_->close();
    return true;
}

bool KrakenDataLogger::scanLastFileState()
{
    Q_ASSERT(file_);
    bool ok;
    if (!logging_ || file_->isOpen())
    {
        lastError_ = "scanLastFileState bad state";
        return false;
    }
    fileVersion_ = 0;
    fileLine_ = 0;
    marketPair_ = "";
    if (!file_->exists())
        return true; // done, new file
    if (!file_->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        lastError_ = QString("scanLastFileState open file for reading failed (%1): %2").arg(fileName_).arg(file_->errorString());
        return false;
    }
    QByteArray baLine;
    while (!file_->atEnd())
    {
        baLine = file_->readLine();
        if (baLine.startsWith("ver,"))
        {
            QStringList parts = QString(baLine).split(',');
            if (parts.size() > 1)
            {
                int ver = parts.at(1).toInt(&ok);
                if (ok)
                    fileVersion_ = ver;
            }
        }
        else if (baLine.startsWith("mkt,"))
        {
            QStringList parts = QString(baLine).split(',');
            if (parts.size() > 1 && !parts.at(1).isEmpty())
                marketPair_ = parts.at(1);
        }
        fileLine_++;
    } // while
    file_->close();
    return true;
}

bool KrakenDataLogger::writeLogEntry(const QString& entry)
{
    Q_ASSERT(file_);
    if (!logging_)
        return true; // non error, just ignore
    if (!leaveOpenLogging_)
    {
        if (!file_->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
        {
            lastError_ = QString("writeLogEntry open file for writing failed (%1): %2").arg(fileName_).arg(file_->errorString());
            return false;
        }
    }
    else if (!file_->isOpen())
    {
        lastError_ = QString("writeLogEntry log file was left open, but is no longer open (%1)").arg(fileName_);
        return false;
    }
    int len = file_->write(entry.toLocal8Bit());
    if (len == -1)
    {
        lastError_ = QString("writeLogEntry file write failed (%1): %2").arg(fileName_).arg(file_->errorString());
        return false;
    }
    if (!leaveOpenLogging_)
        file_->close();
    fileLine_++;
    return true;
}

bool KrakenDataLogger::loadData(const QDateTime& minDate /*= QDateTime()*/, const QString& format /*= QString()*/, quint64 delayMs /*= 0 fastest*/, const QString& fileName /*= QString()*/)
{
    Q_ASSERT(file_);
    Q_ASSERT(loadDelayTimer_);
    if (isActive())
    {
        lastError_ = "loadData already active";
        return false;
    }
    fileVersion_ = 0;
    if (file_->isOpen())
        file_->close();
    if (!format.isEmpty())
        format_ = format;
    if (!fileName.isEmpty())
        fileName_ = fileName;
    if (fileName_.isEmpty())
    {
        lastError_ = "loadData file name not set";
        return false;
    }
    file_->setFileName(fileName_);
    if (!file_->exists())
    {
        lastError_ = QString("loadData file doesn't exist: %1").arg(fileName_);
        return false;
    }
    loading_ = true;
    fileLine_ = 0;
    marketPair_ = "";
    minLoadDate_ = minDate;
    formatFields_.clear();
    if (!file_->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        lastError_ = QString("loadData open file for reading failed (%1): %2").arg(fileName_).arg(file_->errorString());
        stop();
        return false;
    }
    if (delayMs != 0)
        loadDelayTimer_->start(delayMs);
    else
    {
        int len;
        while (!file_->atEnd())
        {
            len = file_->readLine(readBuffer, C_Read_Buffer_Size);
            if (len == -1)
            {
                lastError_ = QString("loadData file readLine failed (%1): %2").arg(fileName_).arg(file_->errorString());
                stop();
                return false;
            }
            if (!inputDataEntry(readBuffer, len))
            {
                lastError_ = QString("loadData file inputDataEntry failed (%1) [line %2]: %3").arg(fileName_).arg(fileLine_).arg(lastError_);
                stop();
                return false;
            }
            fileLine_++;
        }
        stop();
    }
    return true;
}

void KrakenDataLogger::stop()
{
    Q_ASSERT(file_);
    Q_ASSERT(loadDelayTimer_);
    loadDelayTimer_->stop();
    flushLogTimer_->stop();
    file_->close();
    logging_ = loading_ = false;
    leaveOpenLogging_ = false;
    fileVersion_ = 0;
    fileLine_ = 0;
    marketPair_ = "";
}

bool KrakenDataLogger::truncateFile(const QString& fileName /*= QString()*/)
{
    Q_ASSERT(file_);
    if (isActive())
    {
        lastError_ = "truncateFile already active";
        return false;
    }
    fileVersion_ = 0;
    if (file_->isOpen())
        file_->close();
    if (!fileName.isEmpty())
        fileName_ = fileName;
    if (fileName_.isEmpty())
    {
        lastError_ = "truncateFile file name not set";
        return false;
    }
    file_->setFileName(fileName_);
    if (!file_->exists())
    {
        lastError_ = QString("truncateFile file doesn't exist: %1").arg(fileName_);
        return false;
    }
    if (!file_->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
    {
        lastError_ = QString("truncateFile open file for truncating failed (%1): %2").arg(fileName_).arg(file_->errorString());
        return false;
    }
    file_->close();
    return true;
}

bool KrakenDataLogger::deleteFile(const QString& fileName /*= QString()*/)
{
    Q_ASSERT(file_);
    if (isActive())
    {
        lastError_ = "deleteFile already active";
        return false;
    }
    fileVersion_ = 0;
    if (file_->isOpen())
        file_->close();
    if (!fileName.isEmpty())
        fileName_ = fileName;
    if (fileName_.isEmpty())
    {
        lastError_ = "deleteFile file name not set";
        return false;
    }
    file_->setFileName(fileName_);
    if (!file_->exists())
        return true; // already done
    if (!file_->remove())
    {
        lastError_ = QString("deleteFile file remove failed (%1): %2").arg(fileName_).arg(file_->errorString());
        return false;
    }
    return true;
}

void KrakenDataLogger::loadDelayTimerTick()
{
    Q_ASSERT(file_);
    Q_ASSERT(loadDelayTimer_);
    if (!file_->isOpen())
    {
        stop();
        emit failed("loadData(delayed) file is not open");
        return;
    }
    if (file_->atEnd())
    {
        stop();
        emit finished();
        return;
    }
    int len = file_->readLine(readBuffer, C_Read_Buffer_Size);
    if (len == -1)
    {
        lastError_ = QString("loadData(delayed) file readLine failed (%1): %2").arg(file_->fileName()).arg(file_->errorString());
        stop();
        emit failed(lastError_);
        return;
    }
    inputDataEntry(readBuffer, len);
    fileLine_++;
}

void KrakenDataLogger::flushLogTimerTick()
{
    Q_ASSERT(file_);
    Q_ASSERT(flushLogTimer_);
    if (!leaveOpenLogging_)
    {
        stop();
        emit failed("flushLogTimerTick not in leaveOpenLogging mode");
        return;
    }
    if (!file_->isOpen())
    {
        stop();
        emit failed("flushLogTimerTick file is not open");
        return;
    }
    if (!file_->flush())
    {
        stop();
        emit failed(QString("flushLogTimerTick file flush failed: %1").arg(file_->errorString()));
        return;
    }
}

bool KrakenDataLogger::inputDataEntry(const char* str, int len /*= -1*/)
{
    if (format_ == "default")
        return inputDataEntry_Default(str, len);
    else if (format_ == "simply-csv")
        return inputDataEntry_SimplyCSV(str, len);
    else
    {
        lastError_ = QString("inputDataEntry unsupported format '%1'").arg(format_);
        return false;
    }
    return true;
}

bool KrakenDataLogger::inputDataEntry_Default(const char* str, int len /*= -1*/)
{
    Q_ASSERT(str);
    bool ok;
    bool okl[20];
    QString input = QString::fromLocal8Bit(str, len);
    if (input.isEmpty() || input.startsWith('#'))
        return true; // ignored line, empty or comment
    QVector<QStringRef> parts = input.splitRef(',');
    if (parts.isEmpty())
        return true;
    QStringRef entryType = parts.at(0);

    if (entryType == "ver")
    {
        // ver,version
        if (parts.size() < 3)
            emit parseError(QString("inputDataEntry_Default line %1 entry type %2 missing fields in file: %3").arg(fileLine_).arg(entryType).arg(file_->fileName()));
        else
        {
            quint32 ver = parts.at(2).toUInt(&ok);
            if (!ok)
                emit parseError(QString("inputDataEntry_Default line %1 entry type %2 validate fields failed in file: %3").arg(fileLine_).arg(entryType).arg(file_->fileName()));
            else
                fileVersion_ = ver;
        }
    } // ver
    else if (entryType == "mkt")
    {
        // mkt,marketPair
        if (parts.size() < 3)
            emit parseError(QString("inputDataEntry_Default line %1 entry type %2 missing fields in file: %3").arg(fileLine_).arg(entryType).arg(file_->fileName()));
        else
        {
            if (parts.at(2).isEmpty())
                emit parseError(QString("inputDataEntry_Default line %1 entry type %2 validate fields failed in file: %3").arg(fileLine_).arg(entryType).arg(file_->fileName()));
            else
                setMarketPair(parts.at(2).toString());
        }
    }
    else if (entryType == "cdl")
    {
        // cdl,stime,etime,open,high,low,close,vwap,volume,count,[marketPair]
        if (parts.size() < 10)
            emit parseError(QString("inputDataEntry_Default line %1 entry type %2 missing fields in file: %3").arg(fileLine_).arg(entryType).arg(file_->fileName()));
        else
        {
            okl[0] = true; // entryType, set for consistency
            double stime      = parts.at(1).toDouble(&okl[1]) / 1000.0; //  msFromEpoc truncated to secFromEpoc
            double etime      = parts.at(2).toDouble(&okl[2])  / 1000.0; // msFromEpoc diff truncated to secFromEpoc diff
            etime += stime; // etime is a diff, get true value
            const double open       = parts.at(3).toDouble(&okl[3]);
            const double high       = parts.at(4).toDouble(&okl[4]);
            const double low        = parts.at(5).toDouble(&okl[5]);
            const double close      = parts.at(6).toDouble(&okl[6]);
            const double vwap       = parts.at(7).toDouble(&okl[7]);
            const quint32 volume    = parts.at(8).toUInt(&okl[8]);
            const quint32 count     = parts.at(9).toUInt(&okl[9]);

            ok = true;
            for (int i = 0;i < 10;i++)
                if (!okl[i])
                    ok = false;
            if (!ok)
                emit parseError(QString("inputDataEntry_Default line %1 entry type %2 validate fields failed in file: %3").arg(fileLine_).arg(entryType).arg(file_->fileName()));
            else
            {
                if (parts.size() > 10)
                    setMarketPair(parts.at(11).toString());
                if (!minLoadDate_.isValid() || QDateTime::fromMSecsSinceEpoch((qint64)qFloor(etime)).msecsTo(minLoadDate_) < 0)
                    emit outputOHLC(stime,etime,open,high,low,close,vwap,volume,count);
            }
        }
    } // cdlmarketPair_
    else
    {
        if (fileLine_ == 0) // try to detect CSV
        {
            bool hasNumber = false;
            bool hasTitle = false;
            foreach (const QStringRef& col, parts)
            {
                bool ok;
                col.toDouble(&ok); // ignore return, just want ok
                if (ok)
                    hasNumber = true;
                else
                {
                    QString colCopy = col.toString().toLower();
                    if (colCopy == "date"|| colCopy == "open" || colCopy == "high" || colCopy == "low" || colCopy == "close")
                        hasTitle = true;
                }
            }
            if (!hasNumber && hasTitle)
            {
                debugLog("inputDataEntry_Default detected simply CSV format");
                format_ = "simply-csv";
                return inputDataEntry_SimplyCSV(str, len);
            }
        }
        emit parseError(QString("inputDataEntry_Default line %1 entry type %2 unknown in file: %3").arg(fileLine_).arg(entryType).arg(file_->fileName()));
    }
    return true;
}

bool KrakenDataLogger::inputDataEntry_SimplyCSV(const char* str, int len /*= -1*/)
{
    QString input = QString::fromLocal8Bit(str, len);
    if (input.isEmpty() || input.startsWith('#'))
        return true; // ignored line, empty or comment
    QVector<QStringRef> parts = input.splitRef(',');
    if (parts.isEmpty())
        return true;
    if (formatFields_.size() == 0)
    {
        if (parts.size() <= 6)
        {
            lastError_ = "inputDataEntry_SimplyCSV expected a header line with at least 6 fields";
            return false;
        }
        bool isNumeric = false;
        int highestFieldIndex = 0;
        for (int i = 0;i < parts.size();i++)
        {
            double tmp = parts.at(i).toDouble(&isNumeric);
            Q_UNUSED(tmp);
            if (isNumeric)
            {
                lastError_ = "inputDataEntry_SimplyCSV header line not detected";
                return false;
            }
            formatFields_.insert(parts[i].toString().toLower().trimmed(), i);
            if (i > highestFieldIndex)
                highestFieldIndex = i;
        }
        formatFields_.insert("highest-idx", highestFieldIndex);
        if (!formatFields_.contains("date") || !formatFields_.contains("open") || !formatFields_.contains("high") || !formatFields_.contains("low") || !formatFields_.contains("close") || !formatFields_.contains("volume") || !formatFields_.contains("highest-idx"))
        {
            lastError_ = "inputDataEntry_SimplyCSV not all expected field definitions were found in the header line";
            return false;
        }
        return true;
    } // formatFields_ not set
    if (parts.size() <= formatFields_["highest-idx"])
    {
        emit parseError(QString("inputDataEntry_SimplyCSV line %1 missing fields in file: %2").arg(fileLine_).arg(file_->fileName()));
        return true; // non fatal parsing error, continue
    }
    bool okl[8];
    okl[6] = okl[7] = true; // optional fields
    const double open       = parts.at(formatFields_["open"]).toDouble(&okl[1]);
    const double high       = parts.at(formatFields_["high"]).toDouble(&okl[2]);
    const double low        = parts.at(formatFields_["low"]).toDouble(&okl[3]);
    const double close      = parts.at(formatFields_["close"]).toDouble(&okl[4]);
    const double volume    = parts.at(formatFields_["volume"]).toDouble(&okl[5]);
    const double vwap       = (formatFields_.contains("vwap") ? parts.at(formatFields_["vwap"]).toDouble(&okl[6]) : 0.0);
    const quint32 count     = (formatFields_.contains("count") ? parts.at(formatFields_["count"]).toUInt(&okl[7]) : 0);

    QDateTime dt;
    QString strDt = parts.at(formatFields_["date"]).toString();
    dt = QDateTime::fromString(strDt, "yyyyMMdd HHmmss");
    if (!dt.isValid())
        dt= QDateTime::fromString(strDt, "yyyyMMdd");
    if (!dt.isValid())
        dt= QDateTime::fromString(strDt, "yyyyMMdd.0");
    dt.setTimeSpec(Qt::UTC);
    okl[0] = dt.isValid();
    for (int i = 0;i < 8;i++)
        if (!okl[i])
        {
            emit parseError(QString("inputDataEntry_SimplyCSV line %1 validate field %2 failed in file: %3").arg(fileLine_).arg(i).arg(file_->fileName()));
            return true; // non fatal parsing error, continue
        }
    if (!minLoadDate_.isValid() || dt.msecsTo(minLoadDate_) < 0)
    {
        const double dtSecs = (double)dt.toSecsSinceEpoch();
        emit outputOHLC(dtSecs,dtSecs,open,high,low,close,vwap,volume,count);
    }
    return true;
}

void KrakenDataLogger::inputOHLC(double stime, double etime, double open, double high, double low, double close, double vwap, double volume, int count)
{
    QString extraField = "";
    KrakenMarketAPI* market = qobject_cast<KrakenMarketAPI*>(sender());
    if (market)
    {
        if (multiMarket_)
            extraField = QString(",%1").arg(market->marketPair());
        else if (marketPair_ != market->marketPair())
        {
            if (format_ == "custom")
                if (!writeLogEntry(QString("mkt,%1,%2\n").arg(QDateTime::currentMSecsSinceEpoch()).arg(market->marketPair())))
                {
                    stop();
                    emit failed(QString("inputOHLC writeLogEntry (mkt) failed: %1").arg(lastError_));
                    return;
                }
            setMarketPair(market->marketPair());
        }
    }
    if (lastTimeOHLC_ == 0.0)
        lastTimeOHLC_ = etime;
    else if (lastTimeOHLC_ != etime && !lastOutEntryOHLC_.isEmpty())
    {
        if (!writeLogEntry(lastOutEntryOHLC_))
        {
            stop();
            emit failed(QString("inputOHLC writeLogEntry failed: %1").arg(lastError_));
            return;
        }
        lastTimeOHLC_ = etime;
    }
    if (format_ == "custom")
    {
        stime *= 1000.0; // fractional secsFromEpoc to msFromEpoc
        etime *= 1000.0; // fractional secsFromEpoc to msFromEpoc
        etime = (etime - stime); // only keep diff for etime
        lastOutEntryOHLC_ = QString("cdl,%1,%2,%3,%4,%5,%6,%7,%8,%9%10\n")
                .arg(stime, 0, 'f', 0) // truncate msFromEpoc
                .arg(etime, 0, 'f', 0) // truncate msFromEpoc diff
                .arg(open, 0, 'g', 8)
                .arg(high, 0, 'g', 8)
                .arg(low, 0, 'g', 8)
                .arg(close, 0, 'g', 8)
                .arg(vwap, 0, 'g', 8)
                .arg(volume, 0, 'g', -1)
                .arg(count)
                .arg(extraField);
   }
   else if (format_ == "simply-csv")
   {
        lastOutEntryOHLC_ = QString("%1,%2,%3,%4,%5,%6\n")
                                   .arg(QDateTime::fromSecsSinceEpoch(etime).toString("yyyyMMdd HHmmss"))
                                   .arg(open, 0, 'f', 8)
                                   .arg(high, 0, 'f', 8)
                                   .arg(low, 0, 'f', 8)
                                   .arg(close, 0, 'f', 8)
                                   .arg(volume, 0, 'f', -1);
   }
}

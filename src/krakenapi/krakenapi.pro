
QT += websockets

TEMPLATE = lib


SOURCES += \
    krakenapi.cpp \
    krakendatalogger.cpp \
    krakenmarketapi.cpp \
    krakenapisession.cpp \
    krakenrest.cpp \
    krakenws.cpp \

HEADERS += \
    krakenapi.h \
    krakendatalogger.h \
    krakenmarketapi.h \
    krakenapisession.h \
    krakenrest.h \
    krakenws.h \

include(ui/ui.pro)

#ifndef KRAKENMARKETAPI_H
#define KRAKENMARKETAPI_H

#include <QObject>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonArray>
#include <QStringList>

class KrakenAPISession; // predeclared

class KrakenMarketAPI : public QObject
{
    Q_OBJECT
public:
    explicit KrakenMarketAPI(const QString& marketPair, KrakenAPISession* session = nullptr, QObject* parent = nullptr);
    ~KrakenMarketAPI();

    const QString& lastError() const;
    const QString& marketPair() const;
    void setUrls(const QString& urlPublic, const QString& urlPrivate);

    void setSession(KrakenAPISession* session = nullptr);
    bool sessionReady() const;
    KrakenAPISession* session();
    const KrakenAPISession* session() const;

    bool subscribe(const QString& channelName, bool isPublic = true, int reqid = -1, int depth = -1, int interval = -1, bool snapshot = true/*default*/);
    bool unsubscribe(const QString& channelName, bool isPublic = true, int reqid = -1, int depth = -1, int interval = -1);

    bool addOrder(const QString& orderType, const QString& buySell, const QString& price, const QString& volume, int reqid = -1);
    bool cancelOrder(const QString& txid, int reqid = -1);
    bool cancelOrder(const QStringList& txidList, int reqid = -1);
    bool cancelAll(int reqid = -1);

protected:
    QString marketPair_;
    bool ownSession_;
    KrakenAPISession* session_;
    QString lastError_;

    void debugLog(const QString& msg);

signals:
    void debug(QString msg);

    void failed(QString errorMessage);
    void parseError(QString errorMessage);

    // public
    void pubTicker(QJsonDocument msg);
    void pubOHLC(QJsonDocument msg);
    void pubSpread(QJsonDocument msg);
    void pubBook(QJsonDocument msg);
    // private
    void pubOwnTrades(QJsonDocument msg);
    void pubOpenOrders(QJsonDocument msg);
    void pubEvent(QJsonDocument msg);

    // parsed public
    void eventOHLC(double stime, double etime, double open, double high, double low, double close, double vwap, double volume, int count);

public slots:
    void sessionFailed(QString errorMessage);
    void monMessage(QJsonDocument msg);
    void authMessage(QJsonDocument msg);

    void parseOHLC(QJsonDocument msg);

};

#endif // KRAKENMARKETAPI_H

#ifndef KRAKENDATALOGGER_H
#define KRAKENDATALOGGER_H

#include <QObject>
#include <QStringList>
#include <QMap>
#include <QDateTime>

class QFile; // predeclared
class QTimer; // predeclared

class KrakenMarketAPI; // predeclared

#define C_Default_File_Format "default"

class KrakenDataLogger : public QObject
{
    Q_OBJECT
public:
    const static quint32 C_Current_File_Version = 1;
    const static quint32 C_Read_Buffer_Size = 1024;

    explicit KrakenDataLogger(const QString& fileName = QString(), QObject* parent = nullptr);
    ~KrakenDataLogger();

    const QString& lastError() const;
    void debugLog(const QString& msg);

    void setFormat(const QString& format);
    const QString& format() const;

    void setFileName(const QString& fileName);
    const QString& fileName() const;

    bool isActive() const; // logging or loading
    bool isLogging() const;
    bool isLoading() const;

    void setMarket(KrakenMarketAPI* market);
    KrakenMarketAPI* market();

    void setMultiMarket(bool multi = true);
    bool multiMarket() const;

    void setMarketPair(const QString& marketPair);
    const QString& marketPair() const;

    bool logData(const QString& format = QString(), bool leaveOpen = true, const QString& fileName = QString());
    bool loadData(const QDateTime& minDate = QDateTime(), const QString& format = QString(), quint64 delayMs = 0, const QString& fileName = QString());
    void stop();

    bool truncateFile(const QString& fileName = QString());
    bool deleteFile(const QString& fileName = QString());

    bool inputDataEntry(const char* str, int len = -1);
    bool inputDataEntry_Default(const char* str, int len = -1);
    bool inputDataEntry_SimplyCSV(const char* str, int len = -1);


protected:
    bool logging_;
    bool loading_;
    bool leaveOpenLogging_;
    QString format_;
    QMap<QString,int> formatFields_;
    QString fileName_;
    quint32 fileVersion_;
    quint64 fileLine_;
    double lastTimeOHLC_;
    QString lastOutEntryOHLC_;
    KrakenMarketAPI* market_;
    bool multiMarket_;
    QString marketPair_;
    QFile* file_;
    QDateTime minLoadDate_;
    QTimer* loadDelayTimer_;
    QTimer* flushLogTimer_;
    QString lastError_;
    char readBuffer[KrakenDataLogger::C_Read_Buffer_Size];

    bool scanLastFileState();
    bool writeLogEntry(const QString& entry);

signals:
    void failed(QString msg);
    void debug(QString msg);
    void parseError(QString msg);
    void marketPairChanged(QString marketPair);
    void finished();
    void outputOHLC(double stime, double etime, double open, double high, double low, double close, double vwap, double volume, int count);

public slots:
    void loadDelayTimerTick();
    void flushLogTimerTick();
    void inputOHLC(double stime, double etime, double open, double high, double low, double close, double vwap, double volume, int count);

};

#endif // KRAKENDATALOGGER_H

#include "krakenws.h"
#include <QtWebSockets>

KrakenWS::KrakenWS(QWidget *parent) :
    QWidget(parent),
    isOpen_(false),
    wantDisconnect_(false),
    url_("wss://ws.kraken.com"), //"wss://ws-auth.kraken.com/"
    webSocket_(new QWebSocket())
{
    Q_ASSERT(webSocket_);
    connect(webSocket_, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(wsError(QAbstractSocket::SocketError)));
    connect(webSocket_, SIGNAL(textMessageReceived(QString)), this, SLOT(wsTextMessageReceived(QString)));
    connect(webSocket_, SIGNAL(connected()), this, SLOT(wsConnected()));
    connect(webSocket_, SIGNAL(disconnected()), this, SLOT(wsDisconnected()));
}

KrakenWS::~KrakenWS()
{
    wantDisconnect_ = true;
    if (webSocket_)
        {delete webSocket_;webSocket_ = nullptr;}
}

const QString& KrakenWS::lastError() const
{
    return lastError_;
}

void KrakenWS::setUrl(const QUrl& url)
{
    url_ = url;
}

const QUrl& KrakenWS::url() const
{
    return url_;
}

bool KrakenWS::open()
{
    Q_ASSERT(webSocket_);
    if (!url_.isValid())
    {
        lastError_ = "open validate url failed";
        return false;
    }
    wantDisconnect_ = false;
    webSocket_->open(url_);
    return true;
}

void KrakenWS::close()
{
    Q_ASSERT(webSocket_);
    wantDisconnect_ = true;
    webSocket_->close();
}

bool KrakenWS::isOpen()
{
    return isOpen_;
}

void KrakenWS::wsConnected()
{
    isOpen_ = true;
    emit connected();
}

void KrakenWS::wsDisconnected()
{
    isOpen_ = false;
    if (!wantDisconnect_)
    {
        lastError_ = "Unexpected disconnect";
        emit failed(lastError_);
    }
}

void KrakenWS::wsError(QAbstractSocket::SocketError error)
{
    lastError_ = QString("Socket error: %1").arg((int)error);
    emit failed(lastError_);
}

void KrakenWS::wsTextMessageReceived(QString stringMessage)
{
    QJsonParseError parseError;
    QJsonDocument json = QJsonDocument::fromJson(stringMessage.toLocal8Bit(), &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        lastError_ = QString("Replied data has a JSON parsing error: %1").arg(parseError.errorString());
        emit failed(lastError_);
        return;
    }
    emit message(json);
}

bool KrakenWS::sendMessage(const QJsonDocument& jsonMessage)
{
    return sendMessage(QString(jsonMessage.toJson(QJsonDocument::Compact)));
}

bool KrakenWS::sendMessage(const QString& stringMessage)
{
    Q_ASSERT(webSocket_);
    if (!isOpen_)
    {
        lastError_ = "sendMessage failed, socket is not open";
        return false;
    }
    webSocket_->sendTextMessage(stringMessage);
    return true;
}

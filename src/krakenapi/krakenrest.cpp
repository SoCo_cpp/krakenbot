#include "krakenrest.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QCryptographicHash>
#include <QMessageAuthenticationCode>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

KrakenREST::KrakenREST(QObject* parent /*= nullptr*/, QNetworkAccessManager* networkManager /*= nullptr*/) :
    QObject(parent),
    urlPublic_("https://api.kraken.com/0/public/"),
    urlPrivate_("https://api.kraken.com/0/private/")

{
    ownNetMan_ = (networkManager == nullptr);
    if (ownNetMan_)
        netMan_ = new QNetworkAccessManager();
    else
        netMan_ = networkManager;
    Q_ASSERT(netMan_);
    connect(netMan_, SIGNAL(finished(QNetworkReply*)), this, SLOT(netRequestFinished(QNetworkReply*)));
}

KrakenREST::~KrakenREST()
{
    if (ownNetMan_ && netMan_)
        {delete netMan_;netMan_ = nullptr;}
}

const QString& KrakenREST::lastError() const
{
    return lastError_;
}

void KrakenREST::setAuthKeys(const QString& pubKey, const QString& privKey)
{
    keyPublic_ = pubKey;
    keyPrivate_ = privKey;
}
void KrakenREST::setUrls(const QUrl& urlPublic, const QUrl& urlPrivate)
{
    urlPublic_ = urlPublic;
    urlPrivate_ = urlPrivate;;
}

const QUrl& KrakenREST::urlPublic() const
{
    return urlPublic_;
}

const QUrl& KrakenREST::urlPrivate() const
{
    return urlPrivate_;
}

QString KrakenREST::getNonce() const
{
    return QString::number((qlonglong)(QDateTime::currentSecsSinceEpoch() * 1000));
}

void KrakenREST::netRequestFinished(QNetworkReply* reply)
{
    Q_ASSERT(reply);
    QJsonParseError parseError;
    QByteArray baReply = reply->readAll();
    QJsonDocument jsonReply = QJsonDocument::fromJson(baReply, &parseError);
    quint32 reqid = 0;
    if (activeRequests_.contains(reply))
        reqid = activeRequests_.take(reply);
    if (reply->error() != QNetworkReply::NoError)
    {
        lastError_ =  QString("Request replied with error: %1").arg((int)reply->error());
        if (jsonReply.isObject() && jsonReply.object().contains("error"))
            lastError_ += QString(" (%1)").arg(jsonReply.object().value("error").toString());
        emit failed(reqid, lastError_);
    }
    else if (parseError.error != QJsonParseError::NoError)
    {
        lastError_ = QString("Request replied data parse error: %1").arg(parseError.errorString());
        emit failed(reqid, lastError_);
    }
    else emit finished(reqid, jsonReply);
    reply->deleteLater();
}

bool KrakenREST::requestPublic(quint32 reqid, const QString& command, const QMap<QString,QString>& params)
{
    QUrlQuery urlParams;
    foreach (const QString& key, params.keys()) // convert data map to url parameters
        urlParams.addQueryItem(key, params.value(key, ""));
    return requestPublic(reqid, command, urlParams.query());
}

bool KrakenREST::requestPublic(quint32 reqid, const QString& command, const QString& paramAssignments)
{
    Q_ASSERT(netMan_);
    if (!urlPublic_.isValid())
    {
        lastError_ = "requestPublic public url not valid";
        return false;
    }
    QUrl url = urlPublic_;
    url.setPath(url.path() + command.trimmed().toUtf8());  // append command to url path
    url.setQuery(paramAssignments);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setHeader(QNetworkRequest::UserAgentHeader, cUserAgent);
    lastError_ = "success";
    QNetworkReply* netManReply = netMan_->get(request); // initiate HTTPS GET, emits finished or failed
    activeRequests_[netManReply] = reqid;
    return true;
}

bool KrakenREST::requestPrivate(quint32 reqid, const QString& command, const QMap<QString,QString>& params)
{
    QUrlQuery urlParams;
    foreach (const QString& key, params.keys()) // convert data map to url parameters
        urlParams.addQueryItem(key, params.value(key, ""));
    return requestPrivate(reqid, command, urlParams.query());
}

bool KrakenREST::requestPrivate(quint32 reqid, const QString& command, const QString& paramAssignments)
{
    Q_ASSERT(netMan_);
    if (!urlPrivate_.isValid())
    {
        lastError_ = "requestPrivate private url not valid";
        return false;
    }
    if (keyPublic_.isEmpty() || keyPublic_ == "<ENTER PUBLIC API KEY HERE>" || keyPrivate_.isEmpty() || keyPrivate_ == "<ENTER PRIVATE API KEY HERE>")
    {
        lastError_ = "requestPrivate public and private key not set";
        return false;
    }
    QUrl url = urlPrivate_;
    url.setPath(url.path() + command.trimmed().toUtf8()); // append command to url path
    QNetworkRequest request(url);
    QString nonce = getNonce();
    QString postData = QString("nonce=%1").arg(nonce);
    if (!paramAssignments.isEmpty())
        postData = QString("%1&%2").arg(postData).arg(paramAssignments);
    // SHA-256 hash nonce and param post data (inclufing the nonce again)
    QByteArray postHash = QCryptographicHash::hash( QString(nonce + postData.toUtf8()).toUtf8(), QCryptographicHash::Sha256);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setHeader(QNetworkRequest::UserAgentHeader, cUserAgent);
    request.setRawHeader(QByteArray("API-Key"), keyPublic_.trimmed().toUtf8());
    request.setRawHeader(QByteArray("API-Sign"), QMessageAuthenticationCode::hash(
                                                     (url.path().toUtf8() + postHash), // data to hash
                                                     QByteArray::fromBase64(keyPrivate_.trimmed().toUtf8()), // key to hash with
                                                     QCryptographicHash::Sha512 // SHA-512
                                                    ).toBase64()); // base64 the hash when done
    lastError_ = "success";
    QNetworkReply* netManReply = netMan_->post(request, postData.toUtf8()); // initiate HTTPS POST, emits finished or failed
    activeRequests_[netManReply] = reqid;
    return true;
}

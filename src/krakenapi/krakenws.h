#ifndef KRAKENWS_H
#define KRAKENWS_H

#include <QWidget>
#include <QUrl>
#include <QAbstractSocket>
#include <QJsonDocument>

class QWebSocket; // predeclared

class KrakenWS : public QWidget
{
    Q_OBJECT
public:
    explicit KrakenWS(QWidget* parent = nullptr);
    ~KrakenWS();

    const QString& lastError() const;
    void setUrl(const QUrl& url);
    const QUrl& url() const;
    bool open();
    void close();
    bool isOpen();
    bool sendMessage(const QJsonDocument& jsonMessage);
    bool sendMessage(const QString& stringMessage);

protected:
    QString lastError_;
    bool isOpen_;
    bool wantDisconnect_;
    QUrl url_;
    QWebSocket* webSocket_;

signals:
    void connected();
    void disconnected();
    void failed(QString errorMessage);
    void message(QJsonDocument message);

public slots:
    void wsConnected();
    void wsDisconnected();
    void wsError(QAbstractSocket::SocketError error);
    void wsTextMessageReceived(QString stringMessage);
};

#endif // KRAKENWS_H

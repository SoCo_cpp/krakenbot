#include "krakenmarketapi.h"
#include "krakenapisession.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

KrakenMarketAPI::KrakenMarketAPI(const QString& marketPair, KrakenAPISession* session /*= nullptr*/, QObject* parent /* = nullptr*/) :
    QObject(parent),
    marketPair_(marketPair),
    ownSession_(false),
    session_(nullptr)
{
    setSession(session);
    connect(this, SIGNAL(pubOHLC(QJsonDocument)), this, SLOT(parseOHLC(QJsonDocument)));
}

KrakenMarketAPI::~KrakenMarketAPI()
{
    if (ownSession_ && session_)
    {
        ownSession_ = false;
        delete session_;
        session_ = nullptr;
    }
}

const QString& KrakenMarketAPI::lastError() const
{
    return lastError_;
}

void KrakenMarketAPI::debugLog(const QString& msg)
{
    emit debug(msg);
}

const QString& KrakenMarketAPI::marketPair() const
{
    return marketPair_;
}

void KrakenMarketAPI::setSession(KrakenAPISession* session /*= nullptr*/)
{
    if (session_)
    {
        if (ownSession_)
        {
            delete session_;
            session_ = nullptr;
        }
        else disconnect(session_);
    }
    ownSession_ = false;
    session_ = session;
    if (!session_)
    {
        ownSession_ = true;
        session_ = new KrakenAPISession();
    }
    Q_ASSERT(session_);
    connect(session_, SIGNAL(failed(QString)), this, SLOT(sessionFailed(QString)));
    connect(session_, SIGNAL(failed(QString)), this, SIGNAL(failed(QString)));
    connect(session_, SIGNAL(monMessage(QJsonDocument)), this, SLOT(monMessage(QJsonDocument)));
    connect(session_, SIGNAL(authMessage(QJsonDocument)), this, SLOT(authMessage(QJsonDocument)));
}

bool KrakenMarketAPI::sessionReady() const
{
    Q_ASSERT(session_);
    return session_->isReady();
}

KrakenAPISession* KrakenMarketAPI::session()
{
    Q_ASSERT(session_);
    return session_;
}

const KrakenAPISession* KrakenMarketAPI::session() const
{
    Q_ASSERT(session_);
    return session_;
}

void KrakenMarketAPI::monMessage(QJsonDocument msg)
{
    if (msg.isObject())
    {
        QJsonObject jo = msg.object();
        if (jo.contains("event"))
            emit pubEvent(msg);
        else
        {
            lastError_ = "monMessage of object type is unkown";
            emit parseError(lastError_);
            debugLog(QString(msg.toJson(QJsonDocument::Compact)));
        }
        return;
    }
    //----------------------
    if (!msg.isArray())
    {
        lastError_ = "monMessage expected msg to be array";
        emit parseError(lastError_);
        debugLog(QString(msg.toJson(QJsonDocument::Compact)));
        return;
    }
    QJsonArray ja = msg.array();
    if (ja.count() < 3)
    {
        lastError_ = "monMessage expected msg array to contain at least 3 elements";
        emit parseError(lastError_);
        debugLog(QString(msg.toJson(QJsonDocument::Compact)));
        return;
    }
    int channelID = ja.at(0).toInt(-1);
    QString pair = ja.last().toString();
    QString channelName = ja.at(ja.count() - 2).toString();
    if (pair != marketPair_)
    {
        debugLog(QString("market '%1' ignnoring monMessage for other pair ch id %1, pair '%2', chan '%3'").arg(marketPair_).arg(channelID).arg(pair).arg(channelName));
        return;
    }
    //debugLog(QString("market monMessage ch id %1, pair '%2', chan '%3'").arg(channelID).arg(pair).arg(channelName));
    if (channelID == 0 && channelName == "trade")
        emit pubTicker(msg);
    else if (channelName.startsWith("ohlc-"))
        emit pubOHLC(msg);
    else if (channelID == 0 && channelName == "spread")
        emit pubSpread(msg);
    else if ((channelID == 0 && channelName == "book-100") || (channelID == 1234 && (channelName == "book-10" || channelName == "book-25")))
        emit pubBook(msg);

}

void KrakenMarketAPI::authMessage(QJsonDocument msg)
{
    if (msg.isObject())
    {
        QJsonObject jo = msg.object();
        if (jo.contains("event"))
            emit pubEvent(msg);
        else
        {
            lastError_ = "authMessage of object type is unkown";
            emit parseError(lastError_);
            debugLog(QString(msg.toJson(QJsonDocument::Compact)));
        }
        return;
    }
    //----------------------
    if (!msg.isArray())
    {
        lastError_ = "authMessage expected msg to be array or object";
        emit parseError(lastError_);
        debugLog(QString(msg.toJson(QJsonDocument::Compact)));
        return;
    }
    QJsonArray ja = msg.array();
    if (ja.count() < 3)
    {
        lastError_ = "authMessage expected msg array to contain at least 3 elements";
        emit parseError(lastError_);
        debugLog(QString(msg.toJson(QJsonDocument::Compact)));
        return;
    }
    if (ja.contains("event"))
        emit pubEvent(msg);
    else
    {
        QString channelName = ja.at(ja.count() - 2).toString();
        if (channelName == "ownTrades")
            emit pubOwnTrades(msg);
        else if (channelName == "openOrders" || channelName == "openOrders")
            emit pubOpenOrders(msg);
        else
        {
            lastError_ = QString("authMessage unknown channel: %1").arg(channelName);
            emit parseError(lastError_);
        }
    }
}

bool KrakenMarketAPI::subscribe(const QString& channelName, bool isPublic /*= true*/, int reqid /*= -1*/, int depth /*= -1*/, int interval /*= -1*/, bool snapshot /*= true default*/)
{
    Q_ASSERT(session_);
    if (!session_->subscribe(channelName, isPublic, marketPair_, reqid, depth, interval, snapshot))
    {
        lastError_ = QString("unsubscribe session subscribe failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenMarketAPI::unsubscribe(const QString& channelName, bool isPublic /*= true*/, int reqid /*= -1*/, int depth /*= -1*/, int interval /*= -1*/)
{
    Q_ASSERT(session_);
    if (!session_->unsubscribe(channelName, isPublic, marketPair_, reqid, depth, interval))
    {
        lastError_ = QString("unsubscribe session unsubscribe failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenMarketAPI::addOrder(const QString& orderType, const QString& buySell, const QString& price, const QString& volume, int reqid /*= -1*/)
{
    Q_ASSERT(session_);
    QJsonDocument json;
    QJsonObject jo;
    if (!session_->isReady())
    {
        lastError_ = QString("addOrder session not ready");
        return false;
    }
    jo["event"] = "addOrder";
    jo["ordertype"] = orderType;
    jo["pair"] = marketPair_;
    jo["token"] = session_->authToken();
    jo["type"] = buySell;
    jo["volume"] = volume;
    if (!price.isEmpty())
        jo["price"] = price;
    if (reqid != -1)
        jo["reqid"] = reqid;
    json.setObject(jo);
    if (!session_->sendMessage(false/*isPublic*/, json))
    {
        lastError_ = QString("addOrder session sendMessage (private) failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

bool KrakenMarketAPI::cancelOrder(const QString& txid, int reqid /*= -1*/)
{
    return cancelOrder(QStringList(txid), reqid);
}

bool KrakenMarketAPI::cancelOrder(const QStringList& txidList, int reqid /*= -1*/)
{
    Q_ASSERT(session_);
    QJsonDocument json;
    QJsonObject jo;
    QJsonArray ja;
    if (!session_->isReady())
    {
        lastError_ = QString("cancelOrder session not ready");
        return false;
    }
    jo["event"] = "cancelOrder";
    jo["token"] = session_->authToken();
    if (reqid != -1)
        jo["reqid"] = reqid;
    foreach(const QString& txid, txidList)
        ja.append(QJsonValue(txid));
    jo["txid"] = ja;
    json.setObject(jo);
    if (!session_->sendMessage(false/*isPublic*/, json))
    {
        lastError_ = QString("cancelOrder session sendMessage (private) failed: %1").arg(session_->lastError());
        return false;
    }
    return true;
}

void KrakenMarketAPI::sessionFailed(QString errorMessage)
{
    Q_UNUSED(errorMessage);
    // handle specific fialures
}

void KrakenMarketAPI::parseOHLC(QJsonDocument msg)
{
    //debugLog("market parsing OHLC");
    if (!msg.isArray())
    {
        lastError_ = "parseOHLC base message is not an array";
        emit parseError(lastError_);
        return;
    }
    QJsonArray jaBase = msg.array();
    if (jaBase.size() < 2 || !jaBase[1].isArray())
    {
        lastError_ = "parseOHLC base message's data array not found";
        emit parseError(lastError_);
        return;
    }
    QJsonArray jaData = jaBase[1].toArray();
    if (jaData.size() < 9)
    {
        lastError_ = QString("parseOHLC data array has too few fiels: %1 of 9").arg(jaData.size());
        emit parseError(lastError_);
        return;
    }
    // begin-time, end-time, open, high, low, close, vwap, volume, trade-count
    const double stime = jaData[0].toString().toDouble(); // seconds since epoch
    const double etime = jaData[1].toString().toDouble(); // seconds since epoch
    const double open = jaData[2].toString().toDouble();
    const double high = jaData[3].toString().toDouble();
    const double low = jaData[4].toString().toDouble();
    const double close = jaData[5].toString().toDouble();
    const double vwap = jaData[6].toString().toDouble();
    const double volume = jaData[7].toString().toDouble();
    const int count = jaData[8].toString().toDouble();

    emit eventOHLC(stime, etime, open, high, low, close, vwap, volume, count);
}


#include "krakenapisession.h"
#include "krakenrest.h"
#include "krakenws.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

KrakenAPISession::KrakenAPISession(QObject* parent /*= nullptr*/) :
    QObject(parent),
    isReady_(false),
    rest_(new KrakenREST()),
    wsPublic_(new KrakenWS()),
    wsPrivate_(new KrakenWS())
{
    Q_ASSERT(rest_);
    Q_ASSERT(wsPublic_);
    Q_ASSERT(wsPrivate_);

    setUrlsWS("wss://ws.kraken.com", "wss://ws-auth.kraken.com");

    connect(rest_, SIGNAL(failed(quint32,QString)), this, SLOT(onRestFailed(quint32,QString)));
    connect(rest_, SIGNAL(failed(quint32,QString)), this, SIGNAL(restFailed(quint32,QString)));
    connect(rest_, SIGNAL(finished(quint32,QJsonDocument)), this, SIGNAL(restMessage(quint32,QJsonDocument)));
    // restReply(quint32,QJsonDocument) is connected to rest_ as-needed

    connect(wsPublic_, SIGNAL(failed(QString)), this, SLOT(onMonFailed(QString)));
    connect(wsPublic_, SIGNAL(failed(QString)), this, SIGNAL(monFailed(QString)));
    connect(wsPublic_, SIGNAL(message(QJsonDocument)), this, SIGNAL(monMessage(QJsonDocument)));
    connect(wsPublic_, SIGNAL(disconnected()), this, SLOT(wsDisconnected()));

    connect(wsPrivate_, SIGNAL(failed(QString)), this, SLOT(onAuthFailed(QString)));
    connect(wsPrivate_, SIGNAL(failed(QString)), this, SIGNAL(authFailed(QString)));
    connect(wsPrivate_, SIGNAL(message(QJsonDocument)), this, SIGNAL(authMessage(QJsonDocument)));
    connect(wsPrivate_, SIGNAL(connected()), this, SLOT(wsAuthConnected()));
    connect(wsPrivate_, SIGNAL(disconnected()), this, SLOT(wsDisconnected()));
}

KrakenAPISession::~KrakenAPISession()
{
    if (rest_)
        {delete rest_;rest_ = nullptr;}
    if (wsPublic_)
        {delete wsPublic_;wsPublic_ = nullptr;}
    if (wsPrivate_)
        {delete wsPrivate_;wsPrivate_ = nullptr;}
}

const QString& KrakenAPISession::lastError() const
{
    return lastError_;
}

void KrakenAPISession::debugLog(const QString& msg)
{
    emit debug(msg);
}

KrakenREST* KrakenAPISession::rest()
{
    Q_ASSERT(rest_);
    return rest_;
}

KrakenWS* KrakenAPISession::publicWebSocket()
{
    Q_ASSERT(wsPublic_);
    return wsPublic_;
}

KrakenWS* KrakenAPISession::privateWebSocket()
{
    Q_ASSERT(wsPrivate_);
    return wsPrivate_;
}

void KrakenAPISession::setUrlsREST(const QString& urlPublic, const QString& urlPrivate)
{
    rest_->setUrls(urlPublic, urlPrivate);
}

void KrakenAPISession::setUrlsWS(const QString& urlPublic, const QString& urlPrivate)
{
    wsPublic_->setUrl(urlPublic);
    wsPrivate_->setUrl(urlPrivate);
}

void KrakenAPISession::setAuthKeys(const QString& pubKey, const QString& privKey)
{
    Q_ASSERT(rest_);
    rest_->setAuthKeys(pubKey, privKey);
}

bool KrakenAPISession::start()
{
    if (isReady())
        return true; // already ready
    stop();
    if (!openMonitor())
    {
        lastError_ = QString("start openMonitor failed: %1").arg(lastError_);
        return false;
    }
    if (!authenticate())
    {
        lastError_ = QString("start authenticate failed: %1").arg(lastError_);
        return false;
    }
    return true;
}

void KrakenAPISession::stop()
{
    isReady_ = false;
    closeMonitor();
    closeAuthenticated();
    authToken_.clear();
}

bool KrakenAPISession::isReady()
{
    return isReady_ && isAuthenticated() &&  monitorIsOpen();
}

bool KrakenAPISession::openMonitor()
{
    Q_ASSERT(wsPublic_);
    if (wsPublic_->isOpen())
    {
        lastError_ = "openMonitor public WS is already open";
        return false;
    }
    if (!wsPublic_->open())
    {
        lastError_ = QString("openMonitor public WS open failed: %1").arg(wsPublic_->lastError());
        return false;
    }
    return true;
}

void KrakenAPISession::closeMonitor()
{
    Q_ASSERT(wsPublic_);
    wsPublic_->close();
}

bool KrakenAPISession::monitorIsOpen()
{
    Q_ASSERT(wsPublic_);
    return wsPublic_->isOpen();
}

bool KrakenAPISession::authenticate()
{
    Q_ASSERT(rest_);
    authToken_.clear();
    connect(rest_, SIGNAL(finished(quint32,QJsonDocument)), this, SLOT(restReply(quint32,QJsonDocument)));
    if (!rest_->requestPrivate(0, "GetWebSocketsToken", ""))
    {
        lastError_ = QString("authenticate rest requestPrivate failed: %1").arg(rest_->lastError());
        return false;
    }
    return true;
}

bool KrakenAPISession::isAuthenticated()
{
    return (!authToken_.isEmpty()) && authenticatedIsOpen();
}

bool KrakenAPISession::openAuthenticated(const QString& authToken)
{
    Q_ASSERT(wsPrivate_);
    if (wsPrivate_->isOpen())
    {
        lastError_ = "openAuthenticated private WS is already open";
        return false;
    }
    authToken_ = authToken;
    if (!wsPrivate_->open())
    {
        lastError_ = QString("openAuthenticated private WS open failed: %1").arg(wsPrivate_->lastError());
        return false;
    }
    return true;
}

void KrakenAPISession::closeAuthenticated()
{
    Q_ASSERT(wsPrivate_);
    wsPrivate_->close();
}

bool KrakenAPISession::authenticatedIsOpen()
{
    Q_ASSERT(wsPrivate_);
    return wsPrivate_->isOpen();
}

const QString& KrakenAPISession::authToken() const
{
    return authToken_;
}

bool KrakenAPISession::request(quint32 reqid, bool isPublic, const QString& command, const QMap<QString,QString>& params)
{
    Q_ASSERT(rest_);
    if (isPublic)
    {
        if (!rest_->requestPublic(reqid, command, params))
        {
            lastError_ = QString("request rest requestPublic failed: %1").arg(rest_->lastError());
            return false;
        }
    }
    else // if (isPublic)
    {
        if (!rest_->requestPrivate(reqid, command, params))
        {
            lastError_ = QString("request rest requestPrivate failed: %1").arg(rest_->lastError());
            return false;
        }
    }
    return true;
}

bool KrakenAPISession::subscribe(const QString& channelName, bool isPublic /*= true*/, const QString& marketPair /*= QString()*/, int reqid /*= -1*/, int depth /*= -1*/, int interval /*= -1*/, bool snapshot /*= true default*/)
{
    QJsonDocument json;
    QJsonObject joBase;
    QJsonObject joSub;
    QJsonArray jaPair;

    if ( (!isPublic && !isReady()) || (isPublic && monitorIsOpen()))
    {
        lastError_ = QString("subscribe session not ready");
        return false;
    }
    joSub["name"] = channelName;
    if (depth != -1)
        joSub["depth"] = depth;
    if (interval != -1)
        joSub["interval"] = interval;
    if (!snapshot)
        joSub["snapshot"] = false; // default is true, only for ownTrades
    if (!isPublic)
        joSub["token"] = authToken();
    if (!marketPair.isEmpty())
    {
        jaPair.append(marketPair);
        joBase["pair"] = jaPair; // must be array
    }
    if (reqid != -1)
        joBase["reqid"] = reqid;
    joBase["event"] = "subscribe";
    joBase["subscription"] = joSub;
    json.setObject(joBase);
    if (!sendMessage(isPublic, json))
    {
        lastError_ = QString("subscribe session sendMessage (%1) failed: %2").arg( (isPublic ? "public" : "private") ).arg(lastError_);
        return false;
    }
    return true;
}

bool KrakenAPISession::unsubscribe(const QString& channelName, bool isPublic /*= true*/, const QString& marketPair /*= QString()*/, int reqid /*= -1*/, int depth /*= -1*/, int interval /*= -1*/)
{
    QJsonDocument json;
    QJsonObject joBase;
    QJsonObject joSub;
    QJsonArray jaPair;

    if ( (!isPublic && !isReady()) || (isPublic && monitorIsOpen()))
    {
        lastError_ = QString("unsubscribe session not ready");
        return false;
    }
    joSub["name"] = channelName;
    if (depth != -1)
        joSub["depth"] = depth;
    if (interval != -1)
        joSub["interval"] = interval;
    if (!isPublic)
        joSub["token"] = authToken();
    if (!marketPair.isEmpty())
    {
        jaPair.append(marketPair);
        joBase["pair"] = jaPair; // must be array
    }
    if (reqid != -1)
        joBase["reqid"] = reqid;
    joBase["event"] = "unsubscribe";
    joBase["subscription"] = joSub;
    json.setObject(joBase);
    if (!sendMessage(isPublic, json))
    {
        lastError_ = QString("unsubscribe session sendMessage (%1) failed: %2").arg( (isPublic ? "public" : "private") ).arg(lastError_);
        return false;
    }
    return true;
}

bool KrakenAPISession::request(quint32 reqid, bool isPublic, const QString& command, const QString& paramAssignments)
{
    Q_ASSERT(rest_);
    if (isPublic)
    {
        if (!rest_->requestPublic(reqid, command, paramAssignments))
        {
            lastError_ = QString("request rest requestPublic failed: %1").arg(rest_->lastError());
            return false;
        }
    }
    else // if (isPublic)
    {
        if (!rest_->requestPrivate(reqid, command, paramAssignments))
        {
            lastError_ = QString("request rest requestPrivate failed: %1").arg(rest_->lastError());
            return false;
        }
    }
    return true;
}

bool KrakenAPISession::sendMessage(bool isPublic, const QJsonDocument& jsonMessage)
{
    return sendMessage(isPublic, QString(jsonMessage.toJson(QJsonDocument::Compact)));
}

bool KrakenAPISession::sendMessage(bool isPublic, const QString& stringMessage)
{
    Q_ASSERT(wsPublic_);
    Q_ASSERT(wsPrivate_);
    debugLog(QString("sess sendMessage [%1]: %2").arg( (isPublic ? "public" : "private") ).arg(stringMessage));
    if (isPublic)
    {
        if (!wsPublic_->sendMessage(stringMessage))
        {
            lastError_ = QString("sendMessage WS Public sendMessage failed: %1").arg(wsPublic_->lastError());
            return false;
        }
    }
    else
    {
        if (!isAuthenticated())
        {
            lastError_ = QString("sendMessage not connected and authenticated").arg(wsPublic_->lastError());
            return false;
        }
        if (!wsPrivate_->sendMessage(stringMessage))
        {
            lastError_ = QString("sendMessage WS Private sendMessage failed: %1").arg(wsPrivate_->lastError());
            return false;
        }
    }
    return true;
}

void KrakenAPISession::onRestFailed(quint32 reqid, QString errorMessage)
{
    lastError_ = QString("onRestFailed(%1): %2").arg(reqid).arg(errorMessage);
    emit failed(lastError_);
}

void KrakenAPISession::onMonFailed(QString errorMessage)
{
    lastError_ = QString("onMonFailed: %1").arg(errorMessage);
    emit failed(lastError_);
}

void KrakenAPISession::onAuthFailed(QString errorMessage)
{
    lastError_ = QString("onAuthFailed: %1").arg(errorMessage);
    emit failed(lastError_);
}

void KrakenAPISession::restReply(quint32 reqid, QJsonDocument jsonReply)
{
    if (reqid != 0)
        return; // reply isn't for session's auth request
    if (!jsonReply.isObject())
    {
        lastError_ = "restReply validate reply failed, not a json object";
        emit failed(lastError_);
        return;
    }
    if (jsonReply.object().contains("error") && jsonReply.object().value("error").isArray() && !jsonReply.object().value("error").toArray().isEmpty())
    {
        lastError_ = QString("restReply reply contained errors: %1").arg(jsonReply.object().value("error").toArray().first().toString());
        emit failed(lastError_);
    }
    if (jsonReply.object().contains("result") && jsonReply.object().value("result").isObject() && jsonReply.object().value("result").toObject().contains("token") && jsonReply.object().value("result").toObject().value("token").isString())
    {
        authToken_ = jsonReply.object().value("result").toObject().value("token").toString();
        if (!authToken_.isEmpty())
        {
            disconnect(rest_, SIGNAL(finished(quint32,QJsonDocument)), this, SLOT(restReply(quint32,QJsonDocument)));
            if (!openAuthenticated(authToken_))
            {
                lastError_ = QString("restReply openAuthenticated failed: %1").arg(lastError_);
                emit failed(lastError_);
            }
            return;
        }
    }
}

void KrakenAPISession::wsDisconnected()
{
    if (isReady_)
    {
        isReady_ = false;
        emit notReady();
    }
}

void KrakenAPISession::wsAuthConnected()
{
    if (!isReady_)
    {
         isReady_ = true;
         emit ready();
    }
}

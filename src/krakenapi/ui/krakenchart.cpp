#include "krakenchart.h"
#include <krakenmarketapi.h>
#include <QChartView>
#include <QtCharts/QCandlestickSeries>
#include <QtCharts/QCandlestickSet>
#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QDateTimeAxis>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include <QtMath>
#include <QFile>
#include <QTimer>
#include <QVBoxLayout>


KrakenChart::KrakenChart(KrakenMarketAPI* market /*= nullptr*/, QWidget* parent /*= nullptr*/) :
    QWidget(parent),
    chartView_(new QtCharts::QChartView(this)),
    candlestickSeries_(new QtCharts::QCandlestickSeries()),
    market_(nullptr),
    intervalSecs_(60.0),
    lastIntervalTimestamp_(0.0),
    minValue_(0.0),
    maxValue_(0.0),
    minDate_(QDateTime::currentDateTime()),
    maxDate_(QDateTime::currentDateTime().addYears(-20)),
    valueRangeUpdateTimer_(new QTimer())
{
    Q_ASSERT(chartView_);
    Q_ASSERT(candlestickSeries_);
    Q_ASSERT(valueRangeUpdateTimer_);

    connect(valueRangeUpdateTimer_, SIGNAL(timeout()), this, SLOT(onValueRangeUpdateTimerTick()));
    valueRangeUpdateTimer_->setInterval(2000/*ms*/);

    candlestickSeries_->setIncreasingColor(QColor(Qt::green));
    candlestickSeries_->setDecreasingColor(QColor(Qt::red));
    chartView_->chart()->addSeries(candlestickSeries_);

    QtCharts::QDateTimeAxis* axisX = new QtCharts::QDateTimeAxis();
    Q_ASSERT(axisX);
    axisX->setTickCount(10);
    axisX->setTitleText("Date/Time");
    axisX->setFormat("M/d HH:mm");
    axisX->setRange(QDateTime::currentDateTime().addSecs(-1), QDateTime::currentDateTime()); // must initialize range and not be matching
    chartView_->chart()->addAxis(axisX, Qt::AlignBottom);
    candlestickSeries_->attachAxis(axisX);

    QtCharts::QValueAxis* axisY = new QtCharts::QValueAxis();
    axisY->setRange(0, 0.0000001);
    chartView_->chart()->addAxis(axisY, Qt::AlignLeft);
    candlestickSeries_->attachAxis(axisY);

    chartView_->chart()->legend()->hide();
    //chartView_->chart()->legend()->setVisible(true);
    //chartView_->chart()->legend()->setAlignment(Qt::AlignTop);
    chartView_->setRenderHint(QPainter::Antialiasing);

    setMarket(market);

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(chartView_);
    setLayout(mainLayout);
}

const QString& KrakenChart::lastError() const
{
    return lastError_;
}

void KrakenChart::debugLog(const QString& msg)
{
    emit debug(msg);
}

void KrakenChart::setMarket(KrakenMarketAPI* market)
{
    Q_ASSERT(chartView_);
    debugLog("chart setting market...");
    if (market_)
    {
        debugLog(QString("chart Disconnecting from market '%1'").arg(market_->marketPair()));
        disconnect(market_, SIGNAL(eventOHLC(double,double,double,double,double,double,double,double,int)), this, SLOT(inputOHLC(double,double,double,double,double,double,double,double,int)));
    }
    market_ = market;
    if (market_)
    {
        debugLog(QString("chart connecting to market '%1'").arg(market_->marketPair()));
        connect(market_, SIGNAL(eventOHLC(double,double,double,double,double,double,double,double,int)), this, SLOT(inputOHLC(double,double,double,double,double,double,double,double,int)));
        chartView_->chart()->setTitle(market_->marketPair());
    }
    else
    {
        debugLog("chart setMarket to none");
        chartView_->chart()->setTitle("<none>");
    }
}

KrakenMarketAPI* KrakenChart::market()
{
    return market_;
}

void KrakenChart::inputOHLC(double stime, double etime, double open, double high, double low, double close, double vwap, double volume, int count)
{
    Q_UNUSED(etime);
    Q_UNUSED(vwap);
    Q_UNUSED(volume);
    Q_UNUSED(count);
    Q_ASSERT(chartView_);
    Q_ASSERT(candlestickSeries_);
    const double timestamp = stime;
    if ( (timestamp - lastIntervalTimestamp_) < intervalSecs_)
    {
        QtCharts::QCandlestickSet*set = candlestickSeries_->sets().last();
        //debugLog(QString("update timestamp: %1").arg( QDateTime::fromSecsSinceEpoch(lastIntervalTimestamp_).toString()));
        Q_ASSERT(set);
        set->setOpen(open);
        set->setHigh(high);
        set->setLow(low);
        set->setClose(close);
    }
    else
    {
        QDateTime dt = QDateTime::fromMSecsSinceEpoch((timestamp * 1000.0)/* from secToEpoc to msToEpoc, double truncates to qint64*/);
        QtCharts::QCandlestickSet* set = new QtCharts::QCandlestickSet(open, high, low, close, (qreal)(dt.toMSecsSinceEpoch()) );
        Q_ASSERT(set);
        /*
        debugLog(QString("timestamp: %1, (%2,%3,%4,%5) count %6")
                 .arg(dt.toString())
                 .arg(open, 0, 'f', 0)
                 .arg(high, 0, 'f', 0)
                 .arg(low, 0, 'f', 0)
                 .arg(close, 0, 'f', 0)
                 .arg(candlestickSeries_->count()));
        */
        lastIntervalTimestamp_ = timestamp;
        candlestickSeries_->append(set);

        if (dt.secsTo(minDate_) > 0)
            minDate_ = dt;
        if (dt.secsTo(maxDate_) < 0)
            maxDate_ = dt;

        QtCharts::QDateTimeAxis* axisX = qobject_cast<QtCharts::QDateTimeAxis*>(chartView_->chart()->axes(Qt::Horizontal).at(0));
        Q_ASSERT(axisX);
        axisX->setRange(minDate_, maxDate_);
        //debugLog(QString("chart date '%1', min '%2', max '%3'").arg(dt.toString("M/d HH:mm")).arg(minDate_.toString("M/d HH:mm")).arg(maxDate_.toString("M/d HH:mm")));
    }
    updateMinMax(open);
    updateMinMax(high);
    updateMinMax(low);
    updateMinMax(close);
}

void KrakenChart::updateMinMax(double value)
{
    bool changed = false;
    if (value < minValue_ || minValue_ == 0.0)
    {
        minValue_ = value;
        changed = true;
    }
    if (value > maxValue_)
    {
        maxValue_ = value;
        changed = true;
    }
    if (changed)
        valueRangeUpdateTimer_->start();
}

void KrakenChart::onValueRangeUpdateTimerTick()
{
    Q_ASSERT(chartView_);
    valueRangeUpdateTimer_->stop();
    QtCharts::QValueAxis* axisY = qobject_cast<QtCharts::QValueAxis*>(chartView_->chart()->axes(Qt::Vertical).at(0));
    Q_ASSERT(axisY);
    axisY->setRange(minValue_, maxValue_);
}

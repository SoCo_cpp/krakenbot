#ifndef KRAKENCHART_H
#define KRAKENCHART_H

#include <QWidget>
#include <QDateTime>

class KrakenMarketAPI; // predeclared
namespace QtCharts
{
    class QChartView; // predeclared
    class QCandlestickSeries; // predeclared
}
class QTimer;

class KrakenChart : public QWidget
{
    Q_OBJECT
public:
    explicit KrakenChart(KrakenMarketAPI* market = nullptr, QWidget* parent = nullptr);
    const QString& lastError() const;
    void debugLog(const QString& msg);

    void setMarket(KrakenMarketAPI* market);
    KrakenMarketAPI* market();

protected:
    QString lastError_;
    QtCharts::QChartView* chartView_;
    QtCharts::QCandlestickSeries* candlestickSeries_;
    KrakenMarketAPI* market_;
    double intervalSecs_;
    double lastIntervalTimestamp_;
    double minValue_;
    double maxValue_;
    QDateTime minDate_;
    QDateTime maxDate_;
    QTimer* valueRangeUpdateTimer_;

    void updateMinMax(double value);
signals:
    void debug(QString msg);
    void parseError(QString errorMessage);

public slots:
    void onValueRangeUpdateTimerTick();
    void inputOHLC(double stime, double etime, double open, double high, double low, double close, double vwap, double volume, int count);
};

#endif // KRAKENCHART_H

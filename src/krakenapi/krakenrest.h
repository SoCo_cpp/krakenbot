#ifndef KRAKENREST_H
#define KRAKENREST_H

#include <QObject>
#include <QUrl>
#include <QMap>
#include <QJsonDocument>


// predeclare these types, they'll be included in the cpp file
class QNetworkAccessManager;
class QNetworkReply;

class KrakenREST : public QObject
{
    Q_OBJECT
public:
    const char* cUserAgent = "Kraken Qt C++ REST";
    // supply your own network manager for parallel use or don't for automatic
    explicit KrakenREST(QObject* parent = nullptr, QNetworkAccessManager* networkManager = nullptr);
    ~KrakenREST();

    const QString& lastError() const;

    void setAuthKeys(const QString& pubKey, const QString& privKey);
    void setUrls(const QUrl& urlPublic, const QUrl& urlPrivate);

    const QUrl& urlPublic() const;
    const QUrl& urlPrivate() const;

    QString getNonce() const;

    bool requestPublic(quint32 reqid, const QString& command, const QMap<QString,QString>& params);
    bool requestPublic(quint32 reqid, const QString& command, const QString& paramAssignments);
    bool requestPrivate(quint32 reqid, const QString& command, const QMap<QString,QString>& params);
    bool requestPrivate(quint32 reqid, const QString& command, const QString& paramAssignments);

protected:
    QString lastError_;
    bool ownNetMan_;
    QNetworkAccessManager* netMan_;
    QString keyPublic_;
    QString keyPrivate_;
    QUrl urlPublic_;
    QUrl urlPrivate_;
    QMap<QNetworkReply*,quint32> activeRequests_;

signals:
    void failed(quint32 reqid, QString errorString);
    void finished(quint32 reqid, QJsonDocument jsonReply);
public slots:
    void netRequestFinished(QNetworkReply* reply);
};

#endif // KRAKENREST_H

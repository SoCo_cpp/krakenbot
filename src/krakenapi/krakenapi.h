#ifndef KRAKENAPI_H
#define KRAKENAPI_H

#include <QObject>
#include <QMap>
#include <QJsonDocument>

class KrakenMarketAPI; // predeclared
class KrakenAPISession; // predeclared
class QSettings; // predeclared

class KrakenAPI : public QObject
{
    Q_OBJECT
public:
    explicit KrakenAPI(KrakenAPISession* session = nullptr, QObject* parent = nullptr);
    ~KrakenAPI();

    const QString& lastError() const;
    void debugLog(const QString& msg);

    bool loadConfig(bool createSkeleton = true, QString fileName = QString()); // fileName should not include direcory
    bool saveConfig();
    QSettings* getConfig();

    void setDataPath(const QString& fullPath);
    void setDataDirectory(const QString& endDirectory);
    const QString& dataPath() const;

    void setUrlsREST(const QString& urlPublic, const QString& urlPrivate);
    void setUrlsWS(const QString& urlPublic, const QString& urlPrivate);
    void setAuthKeys(const QString& pubKey, const QString& privKey);

    void setSession(KrakenAPISession* session = nullptr);
    bool sessionReady() const;
    KrakenAPISession* session();
    const KrakenAPISession* session() const;

    bool start();
    void stop();
    bool isReady();

    KrakenMarketAPI* getMarket(const QString& marketPair, bool addMissing = false);
    bool deleteMarket(const QString& marketPair, bool failNoExists = true);

    bool subscribe(const QString& channelName, bool isPublic = true, int reqid = -1, int depth = -1, int interval = -1, bool snapshot = true/*default*/);
    bool unsubscribe(const QString& channelName, bool isPublic = true, int reqid = -1, int depth = -1, int interval = -1);

    bool tradeBalance(quint32 reqid, const QString& assetName);
    bool assetInfo(quint32 reqid, const QString& assetName = QString()/*all*/);
    bool assetInfo(quint32 reqid, const QStringList& assetNames);
    bool assetPairs(quint32 reqid, const QString& assetPair = QString()/*all*/, const QString& info = QString()/*all*/);
    bool assetPairs(quint32 reqid, const QStringList& assetPairs, const QString& info = QString()/*all*/);

    bool cancelAllOrders(int reqid = -1);


protected:
    bool ownSession_;
    KrakenAPISession* session_;
    QSettings* config_;
    QMap<QString,KrakenMarketAPI*> markets_;
    QString lastError_;
    QString dataPath_;

signals:
    void debug(QString msg);
    void failed(QString errorMessage);
    void reqestFailed(quint32 reqid, QString errorMessage);
    void requestMessage(quint32 reqid, QJsonDocument msg);
    void ready();
    void createdDefaultConfigFile();
    void monMessage(QJsonDocument msg);
    void authMessage(QJsonDocument msg);
    void message(QJsonDocument msg);

public slots:
    void sessionFailed(QString errorMessage);
};

#endif // KRAKENAPI_H
